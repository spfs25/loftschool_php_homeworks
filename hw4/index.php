<?php

if ( !isset($_SESSION) && !session_id() ) {
    session_start();
}
if (isset($_SESSION['login']))
    header('Location: html/userprofile.php');

$page = 'auth';
$titlepage = 'Авторизация';
include 'html/head.php';
include 'html/navbar.php';
include 'html/auth.php';

