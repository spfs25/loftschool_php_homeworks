function submitRegistration() {
    var formData = new FormData();
    formData.append('login', $("#registration-login").val());
    formData.append('name', $("#registration-name").val());
    formData.append('password', $("#registration-password").val());
    formData.append('password-repeat', $("#registration-password-repeat").val());
    formData.append('age', $("#registration-age").val());
    formData.append('desc', $("#registration-desc").val());
    formData.append('photo', $("#registration-photo").prop('files')[0]);
    $.ajax({
        type: 'POST',
        url: '../php/reg.php',
        processData: false,
        contentType: false,
        data: formData,
        beforeSend: function () {
            document.getElementById('sendregdata').disabled = true;
            console.log('Заблокировали кнопку отправки');
        },
        success: function (data) {
            $('.notify').html(data);
            $('#reg-form')[0].reset();
            document.getElementById('sendregdata').disabled = false;
            alert(data);
        }
    });

}

function submitAuth() {
    var formData = new FormData();
    formData.append('login', $("#auth-login").val());
    formData.append('password', $("#auth-password").val());
    $.ajax({
        type: 'POST',
        url: '../php/auth.php',
        processData: false,
        contentType: false,
        data: formData,
        success: function (data) {
            $('.notify').html(data);
            $('#auth-form')[0].reset();
            alert(data);
            location.reload();
        }
    });

}

function submitUpdate() {
    var formData = new FormData();
    formData.append('age', $("#update-age").val());
    formData.append('name', $("#update-name").val());
    formData.append('desc', $("#update-desc").val());
    formData.append('photo', $("#update-photo").prop('files')[0]);
    $.ajax({
        type: 'POST',
        url: '../php/update_info_in_lk.php',
        processData: false,
        contentType: false,
        data: formData,
        beforeSend: function () {
            document.getElementById('sendupdatedata').disabled = true;
            console.log('Заблокировали кнопку отправки');
        },
        success: function (data) {
            $('.notify').html(data);
            $('#update-form')[0].reset();
            document.getElementById('sendupdatedata').disabled = false;
            alert(data);
            location.reload();
        }
    });

}


