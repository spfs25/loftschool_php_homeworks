<?php
require_once '../php/connect.php';
require_once '../php/functions.php';


print ('
    <table class="table table-bordered">
        <tr>
          <th>Пользователь(логин)</th>
          <th>Имя</th>
          <th>Возраст</th>
          <th>Описание</th>
          <th>Фотография</th>
          <th>Действия</th>
        </tr>');


$all_user_info = get_all_user_info($pdo);


foreach ($all_user_info as $item => $value) {
    echo '<tr>';
    echo '<td>' . $value['login'] . '</td>';
    echo '<td>' . $value['name'] . '</td>';
    echo '<td>' . age($value['age']) . '</td>';
    echo '<td>' . $value['description'] . '</td>';
    echo '<td>' . '<img src="' . $value['link_to_phpto'] . '" width="200" height="200" alt="НЕТ ФОТО">' . '</td>';
    echo '<td>' . '<a href="../php/delete_user.php?id=' . $value['user_id'] . '&link=' . $value['link_to_phpto'] . '">Удалить пользователя</a>' . '</td>';
    echo '</tr>';

}
echo '</table>';