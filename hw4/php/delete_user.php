<?php

//Скрипт для удаления пользователей при нажатии "удалить пользователя" на странице "список пользователей

require_once '../php/connect.php';
session_start();

$id = $_GET["id"];
//удаляем фото пользователя из папки
$link = $_GET["link"];
if (file_exists("$link") == 1) {
    unlink("$link");
}
user_info_del($id, $pdo);
main_info_del($id, $pdo);


header("location: ../html/list.php");


function main_info_del($id, $pdo)
{
    $sql = 'DELETE FROM users WHERE id=:id';
    $stmn = $pdo->prepare($sql);
    $stmn->execute([':id'=>$id]);
}

function user_info_del($id, $pdo)
{
    $sql = 'DELETE FROM user_info WHERE user_id=:id';
    $stmn = $pdo->prepare($sql);
    $stmn->execute([':id'=> $id]);
}
