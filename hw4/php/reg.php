<?php

include_once '../php/functions.php';
include_once '../php/connect.php';

if ($_POST) {
    $errormsg = '';
    //заносим введенный пользователем логин в переменную $login
    if (empty($_POST['login'])) {
        $errormsg .= 'Введите корректный логин</br>';
    } else {
        $login = $_POST['login'];
        //обрабатываем, чтобы теги и скрипты не работали, а также удаляем лишние пробелы
        $login = stripslashes($login);
        $login = htmlspecialchars($login);
        $login = trim($login);
    }

    if ($_POST['password'] != $_POST['password-repeat']) {
        $errormsg .= 'Введенные пароли не совпадают</br>';
    }

    if (empty($_POST['password'])) {
        $errormsg .= 'Введите корректный пароль</br>';

    } else {
        $password = $_POST['password'];
        //обрабатываем, чтобы теги и скрипты не работали, а также удаляем лишние пробелы
        $password = stripslashes($password);
        $password = htmlspecialchars($password);
        $password = trim($password);
//        $password = sha1($password);
        $password = password_hash($password, PASSWORD_DEFAULT);
    }

    if (empty($_POST['name'])) {
        $errormsg .= 'Заполните имя</br>';
    } else {
        $name = $_POST['name'];
        //обрабатываем, чтобы теги и скрипты не работали
        $name = stripslashes($name);
        $name = htmlspecialchars($name);
    }

    if (empty($_POST['age'])) {
        $errormsg .= 'Укажите дату рождения</br>';
    } else {
        $age = $_POST['age'];
    }
//Проверяем, занят ли логин
    $errormsg .= check_Login($pdo, $login);

    $desc = $_POST['desc'];
    //обрабатываем, чтобы теги и скрипты не работали
    $desc = stripslashes($desc);
    $desc = htmlspecialchars($desc);
//    $desc = strip_tags($desc);

    //Типы файлов, которые можно будет загрузить
    $allowed_filetypes = array('.jpg','', '.JPG', '.Jpg', '.png', '.PNG', '.Png');
    //Полное название загружаемого файла
    $filename = $_FILES['photo']['name'];
    //Получаем расширение загружаемого файла
    $ext = substr($filename, strpos($filename, '.'), strlen($filename) - 1);
    //Проверяем, соответсвует ли загружаемый файл разрещенным нами расширениям
    if (!in_array($ext, $allowed_filetypes)) {
        $errormsg .= 'Данный формат не поддерживается.';
    }


    if (!empty($errormsg)) {
//        echo json_encode($errormsg);
        echo $errormsg;
    } else {
        //добавляем логин и пароль в таблицу users и получаем id последнего добавленного юзера
        $userId = post_main_info($pdo, $login, $password);
        // создаём ссылку на фото и тем самым его имя
        $link_to_photo = "../photos/" . $userId . $ext;
        // Добавляем остальную инфу в таблицу user_info + id последнего пользователя, чтобы срастить две таблицы
        post_user_info($pdo, $userId, $age, $name, $desc, $link_to_photo);
        if (!empty($_FILES['photo']['name'])) {
            copy($_FILES['photo']['tmp_name'], $link_to_photo);
        }
//        echo json_encode('Пользователь ' . $login . ' зарегистрирован');
        echo 'Пользователь ' . $login . ' зарегистрирован';
//        header('http://avto.site/html/succes_reg.html');


    }
}







