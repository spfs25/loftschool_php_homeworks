<?

//Получаем логин текущего пользователя

//session_start();
$user_login = $_SESSION['login'];


require_once '../php/connect.php';
require_once '../php/functions.php';


print ('
    <table class="table table-bordered">
        <tr>
          <th>Пользователь(логин)</th>
          <th>Имя</th>
          <th>Возраст</th>
          <th>Описание</th>
          <th>Фотография</th>
        </tr>');


$user_info_for_lk = get_info_for_lk($pdo, $user_login);


foreach ($user_info_for_lk as $item => $value) {
    echo '<tr>';
    echo '<td>' . $value['login'] . '</td>';
    echo '<td>' . $value['name'] . '</td>';
    echo '<td>' . age($value['age']) . '</td>';
    echo '<td>' . $value['description'] . '</td>';
    echo '<td>' . '<img src="' . $value['link_to_phpto'] . '" width="200" height="200" alt="НЕТ ФОТО">' . '</td>';
    echo '</tr>';

}
echo '</table>';