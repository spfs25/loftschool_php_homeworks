<?php

//Подключаемся к БД
include_once '../php/connect.php';

// проверка на существование пользователя с таким же логином

function check_Login($pdo, $login)
{

    $sql = 'SELECT `login` FROM users WHERE `login`=:login';

    //Подготавливаем PDO выражение для SQL запроса
    $stmn = $pdo->prepare($sql);
    $stmn->bindValue(':login', $login, PDO::PARAM_STR);
    $stmn->execute();
    $rows = $stmn->fetchAll(PDO::FETCH_ASSOC);

    if (count($rows) > 0) {
        return 'Извините, введённый вами логин уже зарегистрирован. Введите другой логин.</br>';
    }

}

//Отправка логина и пароля в таблицу users при регистрации

function post_main_info($pdo, $login, $password)
{
    $sql = 'INSERT INTO users(login, password) VALUES (:login,:password)';
    $stmn = $pdo->prepare($sql);
    $stmn->execute([':password' => $password, ':login' => $login]);
    return $pdo->lastInsertId();
}

//Отправка имени, даты рождения, инфы о себе и и ссылки на фото в таблицу user_info

function post_user_info($pdo, $userId, $age, $name, $description = null, $link_to_photo = null)
{
    $sql = 'INSERT INTO user_info(name,user_id,age,description,link_to_phpto) VALUES (:name,:userid,:age,:desc,:link)';
    $stmn = $pdo->prepare($sql);
    $stmn->execute([':name' => $name, ':userid' => $userId, ':age' => $age, ':desc' => $description, ':link' => $link_to_photo]);
}

// Получаем всю инфу из таблицы user_info для скрипта filelist.php

function get_user_info($pdo)
{
    $user_info = $pdo->query("SELECT * FROM user_info");
//    $all_user_info = $pdo->query("SELECT * FROM users UNION ALL SELECT * FROM user_info");
    return $user_info->fetchall(PDO::FETCH_ASSOC);
}

// Получаем инфу из двух таблиц сразу для скрипта userlist.php
function get_all_user_info($pdo)
{
    $all_user_info = $pdo->query("SELECT * FROM user_info LEFT JOIN users ON (users.id = user_info.user_id)");
    return $all_user_info->fetchall(PDO::FETCH_ASSOC);
}

function get_info_for_lk($pdo, $user_login)
{

    $user_info_for_lk = $pdo->query("SELECT * FROM user_info LEFT JOIN users ON (users.id = user_info.user_id) WHERE login = '$user_login'");
    return $user_info_for_lk->fetchall(PDO::FETCH_ASSOC);
}

//Вычисляем возраст для скрипта userlist.php

function age($date)
{
    $date1 = new DateTime(date("Y-m-d"));
    $date2 = new DateTime($date);
    $interval = $date1->diff($date2);
    return $age = $interval->y;

}

//function update_info_in_lk($pdo, $user_login, $age, $name, $description = null, $link_to_photo = null)
//{
//    $user_id = $pdo->query("SELECT users.id FROM users LEFT JOIN user_info ON (users.id = user_info.user_id) WHERE login = '$user_login'");
//    $array = $user_id->fetchall();
//    $user_id = $array[0];
//    $sql = 'UPDATE user_info SET age=:age,name=:name,desc=$description, link_to_phpto=:link WHERE user_id=:id';
//    $stmn = $pdo->prepare($sql);
//    $stmn->execute([':age'=>$age, ':name'=>$name,':desc'=>$description, ':link'=>$link_to_photo, ':id'=>$user_id]);
//}

