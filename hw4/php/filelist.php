<?php
require_once '../php/connect.php';
require_once '../php/functions.php';


print ('
    <table class="table table-bordered">
        <tr>
          <th>Название файла</th>
          <th>Фотография</th>
          <th>Действия</th>
        </tr>');


$all_user_info = get_user_info($pdo);

foreach ($all_user_info as $item => $value) {
    if ($value['link_to_phpto'] != null) {
        echo '<tr>';
        echo '<td>' . basename($value['link_to_phpto']) . '</td>';
        echo '<td>' . '<img src="' . $value['link_to_phpto'] . '" width="200" height="200" alt="НЕТ ФОТО">' . '</td>';
        echo '<td>' . '<a href="../php/delete_photo.php?link=' . $value['link_to_phpto'] . '">Удалить фото</a>' . '</td>';
    }

}
echo '</table>';

