<?php

session_start();

//заносим введенный пользователем логин в переменную $login, если он пустой, то уничтожаем переменную
if (isset($_POST['login'])) {
    $login = $_POST['login'];
}

if ($login == '') {
    unset($login);
}

if (isset($_POST['password'])) {
    $password = $_POST['password'];
}

if ($password == '') {
    unset($password);
}
//если пользователь не ввел логин или пароль, то выдаем ошибку и останавливаем скрипт
if (empty($login) or empty($password)) {
    exit("Вы ввели не всю информацию, заполните все поля!");
}

//если логин и пароль введены,то обрабатываем их, чтобы теги и скрипты не работали
$login = stripslashes($login);
$login = htmlspecialchars($login);
$password = stripslashes($password);
$password = htmlspecialchars($password);

//удаляем лишние пробелы
$login = trim($login);
$password = trim($password);

//$password = sha1($password);

//Подключаемся к БД
include_once '../php/connect.php';

$sql = 'SELECT * FROM users WHERE login=:login';
$stmt = $pdo->prepare($sql);
$stmt->bindValue(':login', $login, PDO::PARAM_STR);
$stmt->execute();

$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

if (count($rows) > 0) {
    if (password_verify($password, $rows[0]['password'])) {
        $_SESSION['login'] = $rows[0]['login'];
        $_SESSION['id'] = $rows[0]['id'];
        echo "Вы успешно вошли на сайт!";
//        header('Location: ../html/list.php');
    } else {
        echo("Извините, введённый вами пароль неверный.");
    }

} else {
    echo('Логин <b>' . $login . '</b> не найден!');
}