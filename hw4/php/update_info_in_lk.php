<?php
session_start();
$user_login = $_SESSION['login'];

include_once '../php/functions.php';
include_once '../php/connect.php';

if ($_POST) {
    $errormsg = '';

    if (empty($_POST['name'])) {
        $errormsg .= 'Заполните имя</br>';
    } else {
        $name = $_POST['name'];
        //обрабатываем, чтобы теги и скрипты не работали
        $name = stripslashes($name);
        $name = htmlspecialchars($name);
    }

    if (empty($_POST['age'])) {
        $errormsg .= 'Укажите дату рождения</br>';
    } else {
        $age = $_POST['age'];
    }

    $desc = $_POST['desc'];
    //обрабатываем, чтобы теги и скрипты не работали
    $desc = stripslashes($desc);
    $desc = htmlspecialchars($desc);

    //Типы файлов, которые можно будет загрузить
    $allowed_filetypes = array('.jpg', '', '.JPG', '.Jpg', '.png', '.PNG', '.Png');
    //Полное название загружаемого файла
    $filename = $_FILES['photo']['name'];
    //Получаем расширение загружаемого файла
    $ext = substr($filename, strpos($filename, '.'), strlen($filename) - 1);
    //Проверяем, соответсвует ли загружаемый файл разрещенным нами расширениям
    if (!in_array($ext, $allowed_filetypes)) {
        $errormsg .= 'Данный формат не поддерживается.';
    }


    if (!empty($errormsg)) {
//        echo json_encode($errormsg);
        echo $errormsg;
    } else {
        $user_id = $pdo->query("SELECT users.id FROM users LEFT JOIN user_info ON (users.id = user_info.user_id) WHERE login = '$user_login'");
        $array = $user_id->fetchall();
        $id = $array[0];
        $id1 = $id[0];
        $link_to_photo = "../photos/" . $id1 . $ext;
        $sql = 'UPDATE user_info SET age=:age,name=:name,description=:desc, link_to_phpto=:link WHERE user_id=:id';
        $stmn = $pdo->prepare($sql);
        $stmn->execute([':age' => $age, ':name' => $name, ':desc' => $desc, ':link' => $link_to_photo, ':id' => $id1]);

        if (!empty($_FILES['photo']['name'])) {
            copy($_FILES['photo']['tmp_name'], $link_to_photo);
        }
        echo "Информация обновлена";
//        print_r($id1);
    }
}