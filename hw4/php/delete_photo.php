<?php

//Скрипт для удаления фото пользователя при нажатии "удалить фото" на страницу "Список файлов"

require_once '../php/connect.php';
session_start();

$link = $_GET["link"];


header("location: ../html/filelist.php");


if (file_exists("$link") == 1) {
    unlink("$link");
    pagedel($link, $pdo);
} else {
    echo "Фото уже удалено, обновите страницу с помощью ctrl+f5";
}


function pagedel($link, $pdo)
{
    $sql = 'UPDATE user_info SET link_to_phpto=NULL WHERE link_to_phpto=:link';
    $stmn = $pdo->prepare($sql);
    $stmn->execute([':link'=>$link]);
}

