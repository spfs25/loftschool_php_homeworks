<?php
session_start();
if (!isset($_SESSION['login']))
//    echo "Вы не вошли в систему, ". '<a href="/">авторизуйтесь!</a>';
    header('Location: ../html/error.php');
?>
<!DOCTYPE html>
<html lang="en">
<?php $titlepage="Список пользователей"; include '../html/head.php'; ?>
<body>
<?php $page = 'userlist'; include '../html/navbar.php'; ?>

<div class="container">
    <?php include_once '../php/userlist.php' ?>
</div><!-- /.container -->


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="/js/main.js"></script>
<script src="/js/bootstrap.min.js"></script>

</body>
</html>
