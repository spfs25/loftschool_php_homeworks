<?php
if (!isset($_SESSION) && !session_id()) {
    session_start();

    if (isset($_SESSION['login'])) {
        header('Location: ../html/userprofile.php');
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<?php
$titlepage = "Регистрация";
include '../html/head.php';
?>

<body>

<?php $page = 'reg';
include '../html/navbar.php'; ?>

<div class="container">

    <div class="form-container">
        <form class="form-horizontal" id="reg-form" enctype="multipart/form-data" action="javascript:void(0);"
              onsubmit="submitRegistration()">
            <div class="form-group">
                <label for="registration-login" class="col-sm-2 control-label">Логин</label>
                <div class="col-sm-10">
                    <input type="text" name="login" class="form-control" id="registration-login" placeholder="Логин">
                </div>
            </div>
            <div class="form-group">
                <label for="registration-password" class="col-sm-2 control-label">Пароль</label>
                <div class="col-sm-10">
                    <input type="password" name="password" class="form-control" id="registration-password"
                           placeholder="Пароль">
                </div>
            </div>
            <div class="form-group">
                <label for="registration-password-repeat" class="col-sm-2 control-label">Пароль (Повтор)</label>
                <div class="col-sm-10">
                    <input type="password" name="re-password" class="form-control" id="registration-password-repeat"
                           placeholder="Пароль">
                </div>
            </div>
            <div class="form-group">
                <label for="registration-name" class="col-sm-2 control-label">Имя</label>
                <div class="col-sm-10">
                    <input type="text" name="name" class="form-control" id="registration-name" placeholder="Имя">
                </div>
            </div>
            <div class="form-group">
                <label for="registration-age" class="col-sm-2 control-label">Дата рождения</label>
                <div class="col-sm-10">
                    <input type="date" name="age" id="registration-age" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label for="registration-desc" class="col-sm-2 control-label">О_себе</label>
                <div class="col-sm-10">
                    <textarea class="form-control" id="registration-desc" name="desc"></textarea>
                </div>
                <div class="form-group">
                    <label for="registration-photo" class="col-sm-2 control-label">Фото</label>
                    <div class="col-sm-10">
                        <input type="file" name="photo" id="registration-photo" class="form-control">
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button id="sendregdata" class="btn btn-default">Зарегистрироваться</button>
                    <br><br>
                    Зарегистрированы? <a href="/">Авторизируйтесь</a>
                </div>
            </div>

        </form>

    </div>

</div><!-- /.container -->


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!--<script src="js/jquery-3.2.1.min.js"></script>-->
<!--<script src="/js/main.js"></script>-->
<script src="../js/bootstrap.min.js"></script>

</body>
</html>
