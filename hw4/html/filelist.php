<?php
session_start();
if (!isset($_SESSION['login']))
    header('Location: ../html/error.php');
?>

<!DOCTYPE html>
<html lang="en">
<?php
$titlepage="Список файлов";
include '../html/head.php';
?>
<body>
<?php $page = 'filelist'; include '../html/navbar.php'; ?>
<script type="text/javascript">
    var li=document.getElementsByTagName('ul')[0].getElementsByTagName('li');
    for(var i in ul) {
        li[i].className='active';
    }
</script>

<div class="container">
    <?php include_once '../php/filelist.php' ?>
</div><!-- /.container -->


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="/js/main.js"></script>
<script src="/js/bootstrap.min.js"></script>

</body>
</html>
