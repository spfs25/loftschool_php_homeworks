<?
if (!isset($_SESSION) && !session_id()) {
    session_start();
}
if (isset($_SESSION['login']))
    header('Location: ../html/list.php');
?>
<!DOCTYPE html>
<html lang="en">
<body>
<div class="container">

    <div class="form-container">
        <form class="form-horizontal" id="auth-form" enctype="multipart/form-data" action="javascript:void(0);" onsubmit="submitAuth()">
            <div class="form-group">
                <label for="auth-login" class="col-sm-2 control-label">Логин</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="auth-login" name="login" placeholder="Логин">
                </div>
            </div>
            <div class="form-group">
                <label for="auth-password" class="col-sm-2 control-label">Пароль</label>
                <div class="col-sm-10">
                    <input type="password" class="form-control" id="auth-password" name="password"
                           placeholder="Пароль">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button id="sendauthdata" class="btn btn-default">Войти</button>
                    <br><br>
                    Нет аккаунта? <a href="/html/reg.php">Зарегистрируйтесь</a>
                </div>
            </div>
        </form>
    </div>

</div><!-- /.container -->


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>


</body>
</html>
