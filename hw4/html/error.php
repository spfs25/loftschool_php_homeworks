<!DOCTYPE html>
<html lang="en">
<?php
$titlepage="Ошибка!";
include '../html/head.php';
?>
<body>
<?php include '../html/navbar.php' ?>
<div class="container">

    <span class="center_text">Вы не вошли в систему, <a href="/">авторизируйтесь</a></span>

</div><!-- /.container -->


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="../js/main.js"></script>
<script src="../js/bootstrap.min.js"></script>

</body>
</html>
