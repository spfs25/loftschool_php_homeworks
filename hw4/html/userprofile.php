<?php
session_start();
if (!isset($_SESSION['login']))
//    echo "Вы не вошли в систему, ". '<a href="/">авторизуйтесь!</a>';
    header('Location: ../html/error.php');
?>
<!DOCTYPE html>
<html lang="en">
<?php $titlepage = "Личный кабинет";
include '../html/head.php'; ?>
<body>
<?php $page = 'userprofile';
include '../html/navbar.php'; ?>

<div class="container">
    <?php include_once '../php/print_userinfo.php' ?>
    <div class="form-lk-container">
        <form class="form-horizontal" id="update-form" enctype="multipart/form-data" action="javascript:void(0);"onsubmit="submitUpdate()">
            <div class="form-group">
                <label for="update-name" class="col-sm-2 control-label">Сменить Имя</label>
                <div class="col-sm-10">
                    <input type="text" name="name" class="form-control" id="update-name" placeholder="Имя">
                </div>
            </div>
            <div class="form-group">
                <label for="update-age" class="col-sm-2 control-label">Новое др</label>
                <div class="col-sm-10">
                    <input type="date" name="age" id="update-age" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label for="update-desc" class="col-sm-2 control-label">Сменить описание</label>
                <div class="col-sm-10">
                    <textarea class="form-control" id="update-desc" name="desc"></textarea>
                </div>
                <div class="form-group">
                    <label for="update-photo" class="col-sm-2 control-label">Новое фото</label>
                    <div class="col-sm-10">
                        <input type="file" name="photo" id="update-photo" class="form-control">
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button id="sendupdatedata" class="btn btn-default">Обновить инфу</button>
                </div>
            </div>

        </form>

    </div>
</div><!-- /.container -->


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="/js/main.js"></script>
<script src="/js/bootstrap.min.js"></script>

</body>
</html>