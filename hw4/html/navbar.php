<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">На главную</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <?php if (!isset($_SESSION['login'])): ?>
                    <li <?php echo ($page == 'auth') ? "class='active'" : "";?>><a href="/">Авторизация</a></li>
                    <li <?php echo ($page == 'reg') ? "class='active'" : "";?>><a href="../html/reg.php">Регистрация</a></li>

                <?php endif; ?>
                <?php if (isset($_SESSION['login'])): ?>
                    <li <?php echo ($page == 'userprofile') ? "class='active'" : "";?>><a href="../html/userprofile.php">Личный кабинет</a></li>
                    <li <?php echo ($page == 'userlist') ? "class='active'" : "";?>><a href="../html/list.php">Список пользователей</a></li>
                    <li <?php echo ($page == 'filelist') ? "class='active'" : "";?>><a href="../html/filelist.php">Список файлов</a></li>
                    <li><a href="../php/session_destroy.php">Выход</a></li>
                <?php endif; ?>
            </ul>
        </div><!--/.nav-collapse -->
    </div>
</nav>