﻿<?php

require_once 'functions.php';

/*--------- task1 --------- */

$var1 = 1;

$arr1 = ['первая строка', 'вторая строка', 'третья строка'];

echo "task1<br>";
echo task1($arr1) . "<br>";

echo task1($arr1, $var1) . "<br>";

/*--------- task2 --------- */
echo "------task2------<br>";
$arr2 = [1, 2, 3, 4, 5];
$str1 = "/";

echo task2($arr2, $str1) . "<br>";

/*--------- task3 --------- */
echo "------task3------<br>";

print_r(task3('+', 1, 1, 2, -3) . "<br>");


/*--------- task4 --------- */

echo "------task4------<br>";
echo task4(8, 8) . "<br>";

/*--------- task5 --------- */

echo "------task5------<br>";
$arr5 = task5("lol");
echo pechat($arr5) . "<br>";

/*--------- task6 --------- */
echo "------task6------<br>";
task6();

/*--------- task7 --------- */
echo "------task7------<br>";
$str71 = "Карл у Клары украл Кораллы";
$str72 = "Две бутылки лимонада";
task7($str71,$str72) . "<br>";

/*--------- task9 --------- */
echo "------task9------<br>";

echo task9() . "<br>";

/*--------- task10 --------- */
echo "------task10------<br>";
echo task10();