﻿<?php


/*--------- task1 --------- */

function task1($arr1, $var = null)
{

    if (isset($var)) {
        return implode(" ", $arr1);
    } else {
        foreach ($arr1 as $key) {
            echo "<p>$key</p></<br>";
        }
    }

}

/*--------- task2 --------- */

function task2($arr, $str)
{
    if (is_array($arr)) {
        switch ($str) {
            case '+':
                echo $arr[0];
                $summ = $arr[0];

                for ($i = 1; $i < count($arr); $i++) {
                    echo "+" . $arr[$i];
                    $summ = $summ + $arr[$i];
                }

                echo "=" . $summ;
                break;

            case '-':
                echo $arr[0];
                $summ = $arr[0];
                for ($i = 1; $i < count($arr); $i++) {
                    echo "-" . $arr[$i];
                    $summ -= $arr[$i];
                }

                echo "=" . $summ;
                break;

            case '*':
                echo $arr[0];
                $summ = $arr[0];

                for ($i = 1; $i < count($arr); $i++) {
                    echo "*" . $arr[$i];
                    $summ *= $arr[$i];
                }

                echo "=" . $summ;
                break;

            case '/':
                echo $arr[0];
                $summ = $arr[0];

                for ($i = 1; $i < count($arr); $i++) {
                    echo "/" . $arr[$i];
                    $summ /= $arr[$i];
                }

                echo "=" . $summ;
                break;

            default:

                echo "Ввод неккоретного символа";
                break;

        }
    }
}

/*--------- task3 --------- */

function task3()
{
    $args = func_get_args(); //все параметры, которые передали в функцию

    if (is_array($args)) {
        switch ($args[0]) {
            case '+':
                echo $args[1];
                $summ = $args[1];

                for ($i = 2; $i < count($args); $i++) {
                    echo "+" . $args[$i];
                    $summ = $summ + $args[$i];
                }

                echo "=" . $summ;
                break;

            case '-':
                echo $args[1];
                $summ = $args[1];
                for ($i = 2; $i < count($args); $i++) {
                    echo "-" . $args[$i];
                    $summ -= $args[$i];
                }

                echo "=" . $summ;
                break;

            case '*':
                echo $args[1];
                $summ = $args[1];

                for ($i = 1; $i < count($args); $i++) {
                    echo "*" . $args[$i];
                    $summ *= $args[$i];
                }

                echo "=" . $summ;
                break;

            case '/':
                echo $args[1];
                $summ = $args[1];

                for ($i = 1; $i < count($args); $i++) {
                    echo "/" . $args[$i];
                    $summ /= $args[$i];
                }

                echo "=" . $summ;
                break;

            default:

                echo "Ввод неккоретного символа";
                break;

        }
    }

}

/*--------- task4 --------- */

function task4($var1, $var2)
{

    if ((is_int($var1)) & (is_int($var2))) {

        echo "<table border='1'>";

        for ($i = 1; $i <= $var1; $i++) {
            echo "<tr>";

            for ($j = 1; $j <= $var2; $j++) {
                echo "<td>" . $i * $j . "</td>";
            }
            echo "</tr>";

        }
        echo "</table>";
    } else {
        echo "В функцию переданы не целые числа!";
    }
}

/*--------- task5 --------- */

function task5($str)
{

    $str = mb_strtolower($str);
    $arr1 = str_split($str);

    $arr2 = array_reverse($arr1);

    function probels($arr)
    {
        for ($i = 0; $i < count($arr); $i++) {
            if ($arr[$i] !== " ") {
                $bezprob[$i] = $arr[$i];
            }
        }

        return $bezprob;
    }

    $arr1bezprob = probels($arr1);
    $arr2bezprob = probels($arr2);

    for ($i = 0; $i <= count($arr1bezprob); $i++) {
        if ($arr1bezprob[$i] !== $arr2bezprob[$i]) {
            return false;
        } else {
            return true;
        }
    }
}

function pechat($var)
{
    if ($var == true) {
        echo "строка является палиндромом";
    } else {
        echo "строка не палиндром";
    }
}

/*--------- task6 --------- */
function task6()
{
    echo date("m.d.Y G:i") . "<br>";
    echo date("d.m.Y H:i:s", mktime(0, 0, 0, 2, 24, 2016)) . "<br>";
}

/*--------- task7 --------- */
function task7($str71, $str72)
{

    echo preg_replace('![А-Я]!u','',$str71). "<br>";

    $str2 = explode(' ', $str72);
    for ($i = 0; $i < count($str2); $i++) {
        if ($str2[$i] == "Две") {
            $str2[$i] = "Три";
        }
    }
    $str3 = implode(' ', $str2);

    echo $str3 . "<br>";
}

/*--------- task9 --------- */
function task9()
{
    $string = file_get_contents('test.txt');
    return $string;
}

function task10()
{
    $file = fopen('anothertest.txt', 'w+');
    fwrite($file, 'Hello again!');
}