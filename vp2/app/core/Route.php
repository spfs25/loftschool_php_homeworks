<?php

namespace Project2;

use Exception;

class Route
{
    public static function start()
    {
        //Контролллер и действие по умолчанию

        $controllerName = 'Main';
        $actionName = 'index';


        $routes = explode('/', $_SERVER['REQUEST_URI']); // Получаем имя контроллера

        if (!empty($routes[1])) {
            $controllerName = ucfirst(strtolower($routes[1])); //Первый символ в верхний регистр, все остальные в нижний (имя файла)
        }

        if (!empty($routes[2])) {
            $actionName = strtolower($routes[2]); //Получили имя действия (метода в контроллере)
        }

        $controllerFile = __DIR__ . "/../controllers/$controllerName.php"; //файл с классом контроллера, подключаем его

        try {
            if (file_exists($controllerFile)) {
                require_once $controllerFile;
            } else {
                throw new Exception("Файл с классом контроллера не найден");
            }

            $className = '\Project2\\Controllers\\' . $controllerName;
            if (class_exists($className)) {
                $controller = new $className();
            } else {
                throw new Exception("Файл с классом контроллера найден, но сам класс ($className) нет");
            }

            if (method_exists($controller, $actionName)) {
                $controller->$actionName(... array_slice($routes, 3)); //задаём параметры метода $action name, за счёт ... добиваемся динамического количества аргументов
            } else {
                throw new Exception("Класс найден, но в нем нет метода $actionName");
            }
        } catch (Exception $e) {
            require_once __DIR__ . '/../../errors/404.php';
        }
    }
}