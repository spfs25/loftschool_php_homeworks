<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('DB_NAME', 'vp3');

/** Имя пользователя MySQL */
define('DB_USER', 'root');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', '');

/** Имя сервера MySQL */
define('DB_HOST', 'localhost');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8mb4');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'lFd8^ g!UEq%OHsRn&>,Lib8!rtGu-27tOGj5LmuL4|*)*Fvuh:`CHz<xNwmb4DX');
define('SECURE_AUTH_KEY',  '?bZi$*1:6{aZa.do^d>arVi;ldWNYy}ciY[@- DAriwEmj 2v%jkW*RugN]&9/{a');
define('LOGGED_IN_KEY',    'mRWIvG[0pb{_m/:yHNB7HFfhiVIqiDZ^K yh24__KHOA48x4$e_:IpsJ#Q%e,(%a');
define('NONCE_KEY',        'T485|DxKl?Kxa.$u+n:L$)=yn;hC;kY7I?E-W%E!<XXg6jm>BrgpTBS@tLUkkH91');
define('AUTH_SALT',        ']rH|gQRd)5|X8f?{LWRp 0/>,[C`^HWK1*yzXlH@XWz]Bj#@%DyzEW&@P!q?KKtF');
define('SECURE_AUTH_SALT', 'Lda+>}zzqx^=dGg^m[#k!t%SrdO!W3:fl5o+vwx(:eGq]:8<lT9(nr(a:x&HlB#c');
define('LOGGED_IN_SALT',   '$.yao;1YH<vIF!v@<9cx[(0=-gO}rirJ_uTd~*AJwtdrxb)]J^V~!U])locH^x9H');
define('NONCE_SALT',       '_5b~s]Rs/NFM>.#COj>bMJ|W#3jz0.8=5qymZS.%Wb9dM-Q}g_.!WJI;>1}ltRo%');

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 *
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');
