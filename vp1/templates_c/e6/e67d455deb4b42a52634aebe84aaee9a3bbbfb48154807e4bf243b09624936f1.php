<?php

/* base.tmpl */
class __TwigTemplate_24c9be2c01d939293f97764274406aed9a46d0dc55a2c501e70e94660aab4433 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!doctype html>
<html lang=\"en\">
<head>
  <meta charset=\"UTF-8\">
  <title>Админка</title>
  ";
        // line 6
        $this->displayBlock('head', $context, $blocks);
        // line 8
        echo "</head>
<body>
<h2>Символическая админка</h2>
<ul>
    <li><a href=\"admin_panel.php?data=users\">Все пользователи</a></li>
    <li><a href=\"admin_panel.php?data=orders\">Все заказы</a></li>
    <li><a href=\"/\">Вернуться на главную</a></li>
</ul>

";
        // line 17
        $this->displayBlock('body', $context, $blocks);
        // line 19
        echo "</body>
</html>";
    }

    // line 6
    public function block_head($context, array $blocks = array())
    {
        // line 7
        echo "  ";
    }

    // line 17
    public function block_body($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "base.tmpl";
    }

    public function getDebugInfo()
    {
        return array (  55 => 17,  51 => 7,  48 => 6,  43 => 19,  41 => 17,  30 => 8,  28 => 6,  21 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "base.tmpl", "C:\\OpnSrvr\\OSPanel\\domains\\php_loft_2017_vp1\\templates\\base.tmpl");
    }
}
