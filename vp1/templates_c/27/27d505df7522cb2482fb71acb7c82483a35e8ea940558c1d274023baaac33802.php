<?php

/* users.tmpl */
class __TwigTemplate_78e6f4876a67f0c5e421877a2d4691bd9a8507892b8f86e0004b06257f759d5e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<table border=\"1\" class=\"table-style\">
    <caption>Все заказы</caption>
    <tr>
        <th>№ заказчика </th>
        <th>Email </th>
        <th>Имя заказчика </th>
        <th>Телефон </th>
    </tr>
       ";
        // line 9
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["table"] ?? null));
        foreach ($context['_seq'] as $context["key"] => $context["value"]) {
            // line 10
            echo "          <tr>
          <td>";
            // line 11
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["value"], "id", array()), "html", null, true);
            echo "</td>
          <td>";
            // line 12
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["value"], "email", array()), "html", null, true);
            echo "</td>
          <td>";
            // line 13
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["value"], "name", array()), "html", null, true);
            echo "</td>
          <td>";
            // line 14
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["value"], "telephone", array()), "html", null, true);
            echo "</td>
          </tr>
       ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['value'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 17
        echo "</table>";
    }

    public function getTemplateName()
    {
        return "users.tmpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  57 => 17,  48 => 14,  44 => 13,  40 => 12,  36 => 11,  33 => 10,  29 => 9,  19 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "users.tmpl", "C:\\OpnSrvr\\OSPanel\\domains\\php_loft_2017_vp1\\templates\\users.tmpl");
    }
}
