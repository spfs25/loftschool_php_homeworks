$(document).ready(function () {

    $('#sendmessage').on('click', function (e) {
        e.preventDefault();
        var name = $('input[name=name]').val();
        var email = $('input[name=email]').val();
        var phone = $('input[name=phone]').val();
        var address = $('textarea[name=address]').val();
        var comment = $('textarea[name=comment]').val();
        var id = $('button[name=id]').val();
        var payment = $('input[name=payment]:checked').val();
        var callback = $('input[name=callback]').prop("checked");


        $.ajax({
            url: '/php/update_order.php',
            type: 'POST',
            dataType: 'json',
            data: {
                name: name,
                email: email,
                phone: phone,
                comment: comment,
                address: address,
                payment: payment,
                id: id,
                callback: callback
            },
            beforeSend: function () {
                document.getElementById('sendmessage').disabled = true;
                $('#sendmessage').val('Отправляю...');
                console.log('Заблокировали кнопку отправки');
            },
            success: function (data) {
                $('.notify').html(data);
                console.log(data);
                $('#update_order-form')[0].reset();
                document.getElementById('sendmessage').disabled = false;
                console.log('Разблокировали кнопку отправки');
                $('#sendmessage').val('Заказать');
                alert(data);
                document.location.href = '/php/admin_panel.php';
            }
        });

    });
});