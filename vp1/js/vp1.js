function submitOrder() {
    var formData = new FormData();
    formData.append('email', $("#order-email").val());
    formData.append('name', $("#order-name").val());
    formData.append('phone', $("#order-phone").val());
    formData.append('street', $("#order-street").val());
    formData.append('home', $("#order-home").val());
    formData.append('part', $("#order-part").val());
    formData.append('appt', $("#order-appt").val());
    formData.append('floor', $("#order-floor").val());
    formData.append('comment', $("#order-comment").val());
    formData.append('payment', $("#order-payment").val());
    formData.append('callback', $("#order-callback").val());
    // formData.append('captcha', grecaptcha.getResponse());
    formData.append('photo', $("#order-photo").prop('files')[0]);
    $.ajax({
        type: 'POST',
        url: '../php/order.php',
        processData: false,
        contentType: false,
        data: formData,
        beforeSend: function () {
            document.getElementById('sendorderdata').disabled = true;
            console.log('Заблокировали кнопку отправки');
        },
        success: function (data) {
            $('.notify').html(data);
            $('#order-form')[0].reset();
            document.getElementById('sendorderdata').disabled = false;
            alert(data);
            location.reload();
        }
    });

}