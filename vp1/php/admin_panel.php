<?php

require_once('connection.php');
require_once '..\vendor\autoload.php';
require_once 'Zakaz.php';
require_once 'User.php';
require_once 'faker.php';

new DB();
$loader = new Twig_Loader_Filesystem('..\templates');
$twig = new Twig_Environment($loader, array(
    'cache' => '..\templates_c'
));

echo $twig->render('base.tmpl');

// Для вывода заказов частично оставил классический php, для вывода пользователей использовал twig

function print_allorders()
{
    new DB();
    $allorders = Zakaz::all();
    foreach ($allorders as $item => $value) {
        echo '<tr>';
        echo '<td>' . $value['id'] . '</td>';
        echo '<td>' . $value['address'] . '</td>';
        echo '<td>' . $value['comment'] . '</td>';
        echo '<td>';
        if ($value['payment'] == 1) {
            echo 'Потребуется сдача';
        } elseif ($value['payment'] == 2) {
            echo 'Оплата картой';
        } else {
            echo '';
        }
        echo '</td>';
        echo '<td>';
        if ($value['callback'] == 1) {
            echo 'да';
        }
        echo '</td>';
        echo '<td>' . '<a href="../php/edit_order.php?id=' . $value['id'] . '&user_id=' . $value['user_id'] . '">Редактировать</a>' . '</td>';
        echo '</tr>';

    }
    echo '</table>';
}

if (!empty($_GET)) {
    switch ($_GET['data']) {
        case 'users':
//            $fake = new Fake();
//            $fake->add_random_users();
            $allusers = User::all();
            $data = ['table' => $allusers];
            echo $twig->render('users.tmpl', $data);
            break;
        case 'orders':
            echo $twig->render('orders.tmpl');
            print_allorders();
            break;
    }
}

