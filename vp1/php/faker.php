<?php

require_once '..\vendor\autoload.php';

use Faker\Factory;

require_once 'connection.php';
//
//require_once 'User.php';
//require_once 'User.php';

class Fake
{

    function add_random_users()
    {
        $faker = Factory::create();

        for ($i = 0; $i < 3; $i++) {
            $user = new User();
            $user->name = $faker->name;
            $user->email = $faker->email;
            $user->telephone = $faker->phoneNumber;
            $user->save();
        }

    }
}

//    function add_random_zakaz_info()
//    {
//        $faker = Factory::create();
//
//        for ($i = 0; $i < 3; $i++) {
//            $zakaz = new Zakaz();
//            $zakaz->address = $faker->address;
//            $zakaz->email = $faker->email;
////            $user->telephone = $faker->mobileNumber;
//            $zakaz->save();
//        }
//
//    }
//}
