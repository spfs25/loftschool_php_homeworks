<?php

require_once('connection.php');
require_once('sendmail.php');
require_once '..\vendor\autoload.php';
require_once 'User.php';
require_once 'Zakaz.php';

$id = $_GET["id"];
$user_id = $_GET["user_id"];
edit($id, $user_id);

function edit($id, $user_id)
{
    new DB();
    $loader = new Twig_Loader_Filesystem('..\templates');
    $twig = new Twig_Environment($loader, array(
        'cache' => '..\templates_c'
    ));
    $order = Zakaz::find($id);
    $user = User::find($user_id);
    $data = [
        'user' => $user,
        'order' => $order];
    echo $twig->render('edit_order.tmpl', $data);
}

