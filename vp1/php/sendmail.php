<?php

// Переделано с помощью composer и ООП
require_once '..\vendor\autoload.php';
require_once('config.php');

use PHPMailer\PHPMailer\PHPMailer;

class Mailer
{

    protected $mail;

    public function __construct()
    {
        $this->mail = new PHPMailer;
        $this->mail->isSMTP();
        $this->mail->Host = SMTPSERVER;
        $this->mail->SMTPAuth = true;
        $this->mail->Username = EMAIL;
        $this->mail->Password = PASS;
        $this->mail->SMTPSecure = 'ssl';
        $this->mail->Port = PORTTCP;


    }

    public function setMessage($address, $mail, $text, $id_order)
    {
        $this->mail->setFrom(EMAIL, 'Заказ №' . $id_order);
        $this->mail->addAddress($mail, 'DarkBeefBurger за 500 рублей');     // Add a recipient
        $this->mail->isHTML(true);
        $this->mail->CharSet = 'UTF-8';
        $this->mail->Subject = 'DarkBeefBurger за 500 рублей, 1 шт';
        $this->mail->Body = '<p>Ваш заказ будет доставлен по адресу: ' . $address . ' <p>Заказ:  DarkBeefBurger за 500 рублей, 1 шт</p><p>' . $text . '</p>';
    }

    public function send()
    {
        $this->mail->send();
//        if(!$this->mail->send()){
//            echo 'Сообщение не отправлено, потому что: '. $this->mail->ErrorInfo;
//        } else{
//            echo 'Сообщение успешно отправлено!';
//        }
    }
}



