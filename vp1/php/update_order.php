<?php

require_once '..\vendor\autoload.php';
require_once('connection.php');
require_once('sendmail.php');
require_once 'User.php';
require_once 'Zakaz.php';

$db = new DB();

new User();
new Zakaz();

if ($_POST) {


    new User();
    new Zakaz();

    $errormsg = '';


    if (empty($_POST['email'])) {
        $errormsg .= 'Введите корректный email</br>';
    } else {
        if (filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
            $email = $_POST['email'];

        } else {
            $errormsg .= 'Введите корректный email</br>';
        }
    }

    if (empty($_POST['phone'])) {
        $errormsg .= 'Введите номер телефона</br>';
    } else {
        $telephone = preg_replace("/[^,.0-9]/", '', $_POST['phone']);
        if (strlen($telephone) < 11) {
            $errormsg .= 'Введите номер телефона</br>';
        }
    }
    if (empty($_POST['name'])) {
        $errormsg .= 'Введите ваше имя';

    } else {
        $name = $_POST['name'];
    }
    if (empty($_POST['address'])) {
        $errormsg .= 'Введите адрес доставки';
    } else {
        $address = $_POST['address'];
    }

    if ($_POST['callback'] === 'true') {
        $callback = 1;
    } else {
        $callback = null;
    }

    if (!isset($_POST['payment'])) {
        $_POST['payment'] = null;
    }

    $id_user = $_POST['id'];

    if (!empty($errormsg)) {
        echo json_encode($errormsg);
    } else {
        $db->update_user_db($id_user, $name, $email, $telephone);
        $id_order = $db->update_order_db($id_user, $address, $_POST['comment'], $_POST['payment'], $callback);
        echo json_encode('<br>Заказ №' . $id_order . ' изменен.');
    }

}