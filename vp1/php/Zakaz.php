<?php

use Illuminate\Database\Eloquent\Model;

class Zakaz extends Model
{
    public $table = "zakaz";
    public $timestamps = false;
}