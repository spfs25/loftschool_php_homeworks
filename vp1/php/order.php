<?php

use Intervention\Image\ImageManager;

require_once('connection.php');
require_once('sendmail.php');
require_once '..\vendor\autoload.php';
require_once 'User.php';
require_once 'Zakaz.php';
require_once 'validationImage.php';

$mailer = new Mailer();
$db = new DB();

new User();
new Zakaz();

if ($_POST) {
    $errormsg = '';

    if (empty($_POST['email'])) {
        $errormsg .= 'Введите корректный email</br>';
    } else {
        if (filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
            $email = $_POST['email'];

        } else {
            $errormsg .= 'Введите корректный email</br>';
        }
    }

    if (empty($_POST['phone'])) {
        $errormsg .= 'Введите номер телефона</br>';
    } else {
        $telephone = preg_replace("/[^,.0-9]/", '', $_POST['phone']);
        if (strlen($telephone) < 11) {
            $errormsg .= 'Введите номер телефона</br>';
        }
    }
    if (empty($_POST['name'])) {
        $errormsg .= 'Введите ваше имя';

    } else {
        $name = $_POST['name'];
    }

    if ($_POST['callback'] === 'true') {
        $callback = 1;
    } else {
        $callback = null;
    }

    $address = "улица {$_POST['street']}, дом {$_POST['home']}";
    if ($_POST['part'] !== '') {
        $address .= ", корпус {$_POST['part']}";
    } else {
        $errormsg .= 'Введите адрес полностью';
    }
    if ($_POST['appt'] !== '') {
        $address .= ", кв. {$_POST['appt']}";
    } else {
        $errormsg .= 'Введите адрес полностью';
    }
    if ($_POST['floor'] !== '') {
        $address .= ", этаж {$_POST['floor']}";
    } else {
        $errormsg .= 'Введите адрес полностью';
    }

    if (!isset($_POST['payment'])) {
        $_POST['payment'] = null;
    }

    $remoteIp = $_SERVER['REMOTE_ADDR'];
//
//    $gRecaptchaResponse = $_POST["captcha"];
//    $recaptcha = new \ReCaptcha\ReCaptcha("6LfNCToUAAAAAJdLxVjBepRLdBP53fuo9hQ52fMc");
//    $resp = $recaptcha->verify($gRecaptchaResponse, $remoteIp);

//    if (!$resp->isSuccess()) {
//        $errormsg .= "Капча не пройдена";
//    }

    if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
        $remoteIp = array_pop(explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']));
    }


    $user = User::where('email', '=', $email)->first();
    $user_id = $user['id'];

    $uploadedImage = validationImage($_FILES['photo'], $extension);

    if (!empty($errormsg)) {
        echo $errormsg;
    } else {
        $text = '';

        if ($user) {
            $count_orders = Zakaz::where('user_id', '=', $user_id)->count() + 1;
            $text = 'Спасибо. Это ваш ' . $count_orders . ' заказ';
            if ($uploadedImage) {

                $filename = "$user_id.$extension";
                $path = '../photo/' . $filename;
                $tmp_name = $_FILES['photo']['tmp_name'];
                move_uploaded_file($tmp_name, $path);

                $user = User::find($user_id);
                $user->link_to_photo = $filename;;
                $user->save();
            }
        } else {

            $user_id = $db->post_user_db($email, $name, $telephone, $remoteIp);
            $text = 'Спасибо. Это ваш первый заказ.';

            if ($uploadedImage) {

                $filename = "$user_id.$extension";
                $path = '../photo/' . $filename;
                $tmp_name = $_FILES['photo']['tmp_name'];
                move_uploaded_file($tmp_name, $path);

                $manager = new ImageManager();
                $image = $manager->make($path)->resize(480, 480);
                $image->save();

                $user = User::find($user_id);
                $user->link_to_photo = $filename;;
                $user->save();
            }

        }
        $id_order = $db->post_order_db($user_id, $address, $_POST['comment'], $_POST['payment'], $callback);
        $mailer->setMessage($address, $email, $text, $id_order);
//        $mailer->send();
//        echo json_encode('<br>Ваш заказ №' . $id_order . ' поступил в обработку. Письмо с подробностями отправлено на почту ' . $email . '<br>' . $text);
        echo '<br>Ваш заказ №' . $id_order . ' поступил в обработку. Письмо с подробностями отправлено на почту ' . $email . '<br>' . $text;

    }

}


