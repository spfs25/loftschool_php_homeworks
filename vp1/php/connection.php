<?php

require_once('config.php');

use Illuminate\Database\Capsule\Manager as Capsule;

class DB
{
    protected $capsule;

    public function __construct()
    {

        $this->capsule = new Capsule;

        $this->capsule->addConnection([
            'driver' => 'mysql',
            'host' => DBHOST,
            'database' => DBNAME,
            'username' => DBUSER,
            'password' => DBPASS,
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
        ]);


        $this->capsule->setAsGlobal();
        $this->capsule->bootEloquent();
    }

    public function get_users()
    {

    }

    public function get_orders()
    {

    }

    public function post_user_db($email, $name, $telephone, $ip,$link=null)
    {
        $user = new User();
        $user->name = $name;
        $user->email = $email;
        $user->telephone = $telephone;
        $user->ip = $ip;
        $user->link_to_photo = $link;
        $user->save();
        return $user->id;
    }


    public function post_order_db($user_id, $address, $comment = null, $payment = null, $callback = null)
    {
        $zakaz = new Zakaz();
        $zakaz->user_id = $user_id;
        $zakaz->address = $address;
        $zakaz->comment = $comment;
        $zakaz->payment = $payment;
        $zakaz->callback = $callback;
        $zakaz->save();
        return $zakaz->id;

    }

    public function update_user_db($id, $name,$email, $telephone)
    {
        $user = User::find($id);
        $user->name = $name;
        $user->email = $email;
        $user->telephone = $telephone;
        $user->save();

    }

    public function update_order_db($user_id, $address, $comment = null, $payment = null, $callback = null)
    {
        $zakaz = Zakaz::find($user_id);
        $zakaz->user_id = $user_id;
        $zakaz->address = $address;
        $zakaz->comment = $comment;
        $zakaz->payment = $payment;
        $zakaz->callback = $callback;
        $zakaz->save();
        return $zakaz->id;

    }


}










