<?php

require 'php/Car.php';
require 'php/Engine.php';
require 'php/Niva.php';
require 'php/HandBrake.php';
require 'php/Transmission.php';
require 'php/TransmissionManual.php';
require 'php/TransmissionAuto.php';

use Debely\Niva as Niva;
use Debely\TransmissionAuto as TransmissionAuto;

$a = new Niva();
$a->move(175, 175);

echo '<br>', PHP_EOL, 'Тест автоматической коробки', '<br>', PHP_EOL;
$t = new TransmissionAuto();
$t->autoSetGear(15);
$t->autoSetGear(25);
