<?php

namespace Debely;

class Niva extends Car
{
    public $gearBox;
    public $handBrake;
    public $engine;

    public function __construct()
    {
        echo 'Создание автомобиля Нива', '<br>', PHP_EOL;
        $this->engine = new Engine(20, 20);
        $this->gearBox = new TransmissionManual();
        $this->handBrake = new HandBrake();
    }

    public function move(float $distance, float $speed, bool $backward = false)
    {
        if ($speed <= 0 || $distance <= 0) {
            return false;
        }

        $maxspeed = $this->engine->getMaxSpeed();
        if ($maxspeed < $speed) {
            return $this->move($distance, $maxspeed, $backward);
        }

        $this->engine->startEngine();
        $this->handBrake->removeHandBrake();

        if ($backward) {
            echo "Начинаем движение назад со скоростью", "{$speed}м/с", '<br>', PHP_EOL;
        } else {
            if ($speed <= 20) {
                $this->gearBox->first();
            } else {
                $this->gearBox->second();
            }
            echo "Начинаем движение вперед со скоростью ",
            "{$speed}м/с", '<br>', PHP_EOL;
        }

        $distanceCovered = 0;
        while ($distance - $distanceCovered >= 10) {
            $distanceCovered += 10;
            echo "Проехали $distanceCovered метров. ";
            $this->engine->increaseTemperature(5);
        }

        if ($distanceCovered < $distance) {
            echo "Проехали $distance метров. ";
            $this->engine->increaseTemperature(5 * ($distance - $distanceCovered) / 10);
        }

        $this->engine->stopEngine();
        $this->handBrake->setHandBrake();
        $this->gearBox->n();
        return true;
    }
}