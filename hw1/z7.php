﻿<?
$cols = 10;
$rows = 10;
$tr = 1;

echo "<table border='1'>";

for ($i = $tr; $i <= $rows; $i++) {
    echo "<tr>";
    $td = 1;
    for ($j = $td; $j <= $cols; $j++) {
        if ($i % 2 == 0 and $j % 2 == 0) {
            echo "<td>(" . $tr * $td . ")</td>";
        } elseif ($i % 2 != 0 and $j % 2 != 0) {
            echo "<td>[" . $tr * $td . "]</td>";
        } else {
            echo "<td>" . $tr * $td . "</td>";
        }

        $td++;
    }
    echo "</tr>";
    $tr++;
}

echo "</table>";
