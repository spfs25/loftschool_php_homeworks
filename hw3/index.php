﻿<?php

require_once 'functions.php';

/*--------- task1 --------- */
echo "task1</br></br>";
$xml = simplexml_load_file('data.xml');
task1($xml);

/*--------- task2 --------- */
echo "task2</br></br>";
$arr2 = array(
    "var1" => 100,
    "var2" => 200,
    "var3" => 300,
    "var4" => 400,
    "var5" => 500
);
var_dump(task2($arr2));
echo "</br></br>";
/*--------- task2 --------- */
echo "task3</br></br>";
echo task3();
echo "</br></br>";
/*--------- task4 --------- */
echo "task4</br></br>";
task4();
