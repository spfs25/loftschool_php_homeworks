﻿<?php
/*--------- task1 --------- */

function task1($xml)
{

    echo "Order № " . $xml["PurchaseOrderNumber"] . "</br></br>";

    foreach ($xml->Address as $key) {
        printf(
            "%s information:</br>Name: %s</br>Street: %s</br>State: %s</br>Zip: %d</br>County: %s</br></br>",
            $key["Type"],
            $key->Name,
            $key->Street,
            $key->State,
            $key->Zip,
            $key->Country
        );
    }

    echo "Delivery Notes: " . $xml->DeliveryNotes . "</br></br>";
    echo "Items:" . "</br></br>";
    foreach ($xml->Items->Item as $key) {
        printf(
            "Part Number: %s</br>Product Name: %s</br>Quantity: %d</br>US Price: %s$</br>Ship Date: %s</br>Comment: %s</br></br>",
            $key["PartNumber"],
            $key->ProductName,
            $key->Quantity,
            $key->USPrice,
            $key->ShipDate,
            $key->Comment

        );
    }
}


function task2($arr)
{
    file_put_contents('output.json', json_encode($arr));
    $arr2 = $arr;
    json_decode(file_get_contents('output.json'), true);
    if ($x = rand(5, 15) > 9) {
        $arr2['var1'] += 50;
        $arr2['var4'] *= 3;
    }
    file_put_contents('output2.json', json_encode($arr2));
    $arr2 = json_decode(file_get_contents('output2.json'), true);
    $result = array_diff($arr, $arr2);
    return $result;
}

/*--------- task3 --------- */

function task3()
{
    $rows = 5;
    $columns = 10;
    for ($i = 1; $i <= $rows; $i++) {
        for ($j = 1; $j <= $columns; $j++) {
            $row[$j] = rand(1, 100);
        }
        $table [] = $row;
//        echo implode(",", $row) . "</br>";
    }
    $file_csv = fopen('file.csv', 'w');
    foreach ($table as $fields) {
        fputcsv($file_csv, $fields);
    }
    fclose($file_csv);

    $csv_file = fopen('file.csv','r');
    $summ =0;
    while ($data = fgetcsv($csv_file)){
        foreach ($data as $var){
            if($var%2==0){
                $summ +=$var;
            }
        }
    }
    return $summ;

}

function task4(){

    $url = 'https://en.wikipedia.org/w/api.php?action=query&titles=Main%20Page&prop=revisions&rvprop=content&format=json;';
    $ch = curl_init($url);
    curl_exec($ch);
    curl_close($ch);

}





