<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Post extends Model
{

//    protected $fillable = ['title', 'content', 'user_id']; // разршаем поля, которые можно заполнять
    protected $guarded = ['id'];

    public static function storePost($title, $content)
    {
        $post = new Post();

        $post->title = $title;
        $post->content = $content;
        $post->user_id = Auth::id();
        $post->save();
        return $post;
    }
}
