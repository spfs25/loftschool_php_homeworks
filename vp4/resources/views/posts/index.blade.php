@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>
                <a href="/posts/create" class="btn btn-primary">Создание поста</a>
                <div class="panel-body">
                    <ul>
                        @foreach($posts as $post)
                            <a href="/posts/view/{{$post->id}}"><li>{{$post->id}}:{{$post->title}}</li></a>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
