@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Dashboard</div>
                    <a href="/posts/create" class="btn btn-primary">Создание поста</a>
                    <a href="/posts/edit/{{$post->id}}" class="btn btn-primary">Редактировать пост</a>
                    <div class="panel-body">
                       <li>{{$post->id}}</li>
                       <li>{{$post->title}}</li>
                       <li>{{$post->content}}</li>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
